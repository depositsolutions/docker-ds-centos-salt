FROM centos/systemd

RUN yum update -y && yum install -y https://repo.saltstack.com/yum/redhat/salt-repo-2017.7-1.el7.noarch.rpm && \
    yum clean all && yum install epel-release -y && yum upgrade -y && \
    yum install -y \
    sudo \
    udev \
    vim \
    net-tools \
    iproute \
    gnupg \
    openssh-server \
    openssh-clients \
    which \
    curl \
    PyYAML \
    git \
    m2crypto \
    python-jinja2 \
    python-requests \
    python-tornado \
    python-zmq \
    python2-crypto \
    python2-futures \
    python2-msgpack \
    systemd-python \
    virt-what \
    salt-master salt-minion && \
    sed -i 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config && \
    systemctl enable sshd.service
